+++
title = "learn markdown"
date = "2022-07-13T19:53:51+07:00"
author = "TienBuiGia"
authorTwitter = "" #do not include @
cover = "learn_markdown/nhung2.jpg"
tags = ["markdown", "hugo"]
keywords = ["first", "hugo"]
description = "This is my first post using hugo's Quisk start"
showFullContent = false
readingTime = true
hideComments = false
+++

## Hello!

I'm gonna try out markdown things

## TODO:
- [ ] eat.
- [ ] sleep.
- [ ] smash keyboard.

These are some words: _italic_ **bold** normal.

> Anh yeu nhung.
>
> *- me*

First line with two spaces after.  
this is the next line.

this is next paragraph. ***anh yeu Nhung***.

1. item 1 
2. item 2 
3. item 3
	- item 3.1 
	- item 3.2 
	- [X] todo done
	- [ ] not done

[![an image](/images/nhung.jpg "nhung")](https://codeberg.org/tienbuigia)

	#include <stdio.h>
	int main() {
		printf("I love yout\n");
		return 0;
	}

---

horizontal rule. escape: \\\* mother fucker.

```c
#include <stdio.h>
int main() {
	printf("really?\n");
	return 0;
}
```
